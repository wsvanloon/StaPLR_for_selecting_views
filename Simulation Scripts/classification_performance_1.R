### Script for the experiments described in Section 4.2 (StaPLR+ & Group Lasso)
### This script is meant for execution on an SGE/OGS HPC cluster

# Experimental Factors:
# Sample size: 200 or 2000
# Signal percentage: 0 (20 domains); 0.5 (5 domains); 1 (5 domains)
# Number of features per group: 250 or 2500
# Correlation Structure: - rw = 0.1, rb = 0
#                        - rw = 0.4, rb = 0
#                        - rw = 0.4, rb = 0.2

# Fixed Factors:
# Number of domains (views): 30
# Number of signal groups: 10
# Effect size: 0.04
# Number of replicates: 100

# Experimental Conditions
NDOMAINS <- 30
NFEATURES <- c(250, 2500) # (per domain)
SAMPSIZE <- c(200, 2000)
NSIGNAL <- list(c(rep(0, 20), rep(0.5, 5), rep(1, 5))) # change this when number of domains changes
THETA <- c(0.04)
CORS <- list(c(0.1, 0), c(0.4, 0), c(0.4, 0.2))
ALPHABASE <- 0
ALPHAMETA <- 1
STDBASE <- FALSE
STDMETA <- FALSE
BASEBOUNDL <- -Inf
BASEBOUNDU <- Inf
METABOUNDL <- 0 # non-negativity constraints: yes (0) / no (-Inf)
METABOUNDU <- Inf
CVLOSS <- "deviance"
CVLAMBDA <- "lambda.min"
CVPARA <- FALSE
LAMBRAT <- 0.0001
MYSEED <- NA
NORMALIZE <- TRUE
reps <- 1:100

conds <- expand.grid(rep=reps, d=NDOMAINS, mbase=NFEATURES, n=SAMPSIZE, s=NSIGNAL, theta=THETA,
                     cors=CORS, alpha1=ALPHABASE, alpha2=ALPHAMETA, stdbase=STDBASE,
                     stdmeta=STDMETA, ll1=BASEBOUNDL, ul1=BASEBOUNDU, ll2=METABOUNDL,
                     ul2=METABOUNDU, cvloss=CVLOSS, cvlambda=CVLAMBDA, cvparallel=CVPARA,
                     lambdaratio=LAMBRAT, myseed=MYSEED, normalize=NORMALIZE)

# Generate Seed List
set.seed(210318)
seed.list <- sample(.Machine$integer.max, size = nrow(conds))
conds$seed <- seed.list
conds <- conds[order(conds$mbase), ]

# Simulation Function
sim.domain.select <- function(tid, arguments){
  
  # Load Required Libraries
  require(glmnet)
  require(gglasso)
  require(caret)
  
  # Disallow early path termination
  glmnet.control(fdev=0)
  
  # Source Required Scripts
  source("SDLglmnet.R")
  source("blockcorrelate.R")
  source("popscale.R")
  source("sim_train_test.R")
  
  argument <- arguments[tid, ]
  
  d <- argument$d
  mbase <- argument$mbase
  n <- argument$n
  s <- unlist(argument$s)
  theta <- argument$theta
  cors <- unlist(argument$cors)
  rw <- cors[1]
  rb <- cors[2]
  alpha1 <- argument$alpha1
  alpha2 <- argument$alpha2 
  myseed <- argument$myseed
  std.base <- argument$stdbase
  std.meta <- argument$stdmeta
  ll1 <- argument$ll1
  ul1 <- argument$ul1
  ll2 <- argument$ll2
  ul2 <- argument$ul2
  cvloss <- as.character(argument$cvloss)
  cvlambda <- as.character(argument$cvlambda)
  cvparallel <- argument$cvparallel
  normalize <- argument$normalize
  lambdaratio <- argument$lambdaratio
  
  # Simulate Data
  set.seed(argument$seed)
  
  m <- rep(mbase, d)
  
  randomizer <- sample(1:d)
  
  sim.train.test(d=d, m=m[randomizer], ntrain=n, ntest=1000, s=s[randomizer], theta=theta, rw=rw, rb=rb, std=normalize)
  gc()
  
  ## Start Timer
  timer0 <- proc.time()[3]
  
  # SDL Domain Selection
  sdl.obj <- SDLglmnet(Domains=dat$D, y=dat$y, alpha1=alpha1, alpha2=alpha2, myseed=myseed,
                       std.base=std.base, std.meta=std.meta, ll1=ll1, ul1=ul1,
                       ll2=ll2, ul2=ul2, cvloss=cvloss, cvlambda=cvlambda,
                       cvparallel=cvparallel, lambda.ratio=lambdaratio)
  selected.domains <- which(coef(sdl.obj$meta, s = cvlambda)[-1] != 0)
  tpr.sdl <- mean(dat$td %in% selected.domains)
  fpr.sdl <- sum(!(selected.domains %in% dat$td))/(d - sum(s > 0))
  
  ## Stop Timer
  timer1 <- proc.time()[3]
  timer <- round(timer1 - timer0)
  seconds <- timer %% 60
  minutes <- (timer %/% 60) %% 60
  hours <- (timer %/% 60) %/% 60
  timer <- c(hours, minutes, seconds)
  names(timer) <- c("hours", "minutes", "seconds")
  sdl.time <- timer
  
  ## Start Timer
  timer0 <- proc.time()[3]
  
  # Group Lasso Domain Selection
  ggl.x <- do.call(cbind, dat$D)
  ggl.y <- ifelse(dat$y < 1, -1, 1)
  ggl.ind <- rep(1:d, m[randomizer])
  ggl.obj <- cv.gglasso(x=ggl.x, y=ggl.y, group=ggl.ind, pred.loss="loss", nfolds=10, loss="logit", lambda.factor=0.05, nlambda=100)
  ggl.coefs <- coef(ggl.obj, s = cvlambda)[-1]
  group.sums.ggl <- c()
  for(j in 1:d){
    group.sums.ggl[j] <- sum(abs(ggl.coefs[ggl.ind == j]))
  }
  selected.domains.ggl <- which(group.sums.ggl != 0)
  tpr.ggl <- mean(dat$td %in% selected.domains.ggl)
  fpr.ggl <- sum(!(selected.domains.ggl %in% dat$td))/(d - sum(s > 0))
  
  
  ## Stop Timer
  timer1 <- proc.time()[3]
  timer <- round(timer1 - timer0)
  seconds <- timer %% 60
  minutes <- (timer %/% 60) %% 60
  hours <- (timer %/% 60) %/% 60
  timer <- c(hours, minutes, seconds)
  names(timer) <- c("hours", "minutes", "seconds")
  ggl.time <- timer
  
  # SDL predictions
  sdl.preds <- predict.SDLglmnet(object=sdl.obj, newx=testset$D, metadat="response", predtype="response", cvlambda=cvlambda)
  
  # Group lasso predictions
  ggl.xtest <- do.call(cbind, testset$D)
  ggl.preds <- 1/(1 + exp(-predict(object=ggl.obj, newx=ggl.xtest, type="link", s=cvlambda)))
  
  # Collect predictions and true labels in a matrix
  all.predictions <- cbind(sdl.preds, ggl.preds, testset$p, testset$y)
  colnames(all.predictions) <- c("SDL", "gglasso", "true probability", "class label")
  
  # Create Output Object
  out.measures <- c(tpr.sdl, tpr.ggl, fpr.sdl, fpr.ggl)
  names(out.measures) <- c("TPR (SDL)", "TPR (gglasso)", "FPR (SDL)", "FPR (gglasso)")
  
  out.times <- rbind(sdl.time, ggl.time)
  rownames(out.times) <- c("Time (SDL)", "Time (gglasso)")
  
  out.domainselect <- list("Selected (SDL)"=selected.domains, "Selected (gglasso)"=selected.domains.ggl, "Truth"=dat$s)
  
  out.predictions <- all.predictions
  
  out <- list("rates"=out.measures, "timers"=out.times, "selected"=out.domainselect, "predictions"=out.predictions)
  
  return(out)
}


# Perform Simulations
# TID <- as.numeric(Sys.getenv("SGE_TASK_ID"))
# results <- list()
# results$param <- conds[TID,]
# results$out <- sim.domain.select(tid = TID, arguments = conds)
# save(results, file=paste0(TID, ".RData"))
# warnings()


