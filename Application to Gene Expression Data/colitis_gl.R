### Application of Group Lasso to the colitis data with C1 genesets
### For execution as job array on OGS HPC cluster
### Repeated k-fold cross-validation (50 times)

# Preliminaries
rm(list=ls())
load("colitis.RData")
library(caret)
library(gglasso)

# What lambda to use for model selection
cvlambda <- "lambda.min"

# Format data for use in group lasso
y <- ifelse(y < 1, -1, 1)
feature.matrix <- feature.matrix[,order(group.index)]
group.index <- group.index[order(group.index)]

# Set seed
set.seed(120618)
seed.list <- sample(.Machine$integer.max/2, size = 50)
TID <- as.numeric(Sys.getenv("SGE_TASK_ID"))
set.seed(seed.list[TID])

# Outer cross-validation parameters
nfolds <- 2
folds <- createFolds(y, nfolds, list=FALSE)
ggl.preds <- rep(NA, length(y))

ngroups <- rep(NA, nfolds)
nfeatures <- rep(NA, nfolds)

## Start Timer
timer0 <- proc.time()[3]

## Fit models and generate predictions
for(k in 1:nfolds){
  ggl.obj <- cv.gglasso(feature.matrix[folds != k, ], y[folds != k], group.index, pred.loss="loss", nfolds=10, loss="logit", lambda.factor=0.05, nlambda=100)
  ggl.preds[folds == k] <- 1/(1 + exp(-predict(object=ggl.obj, newx=feature.matrix[folds == k,], type="link", s=cvlambda)))
  
  # Calculate number of selected groups
  ggl.coefs <- coef(ggl.obj, s ="lambda.min")[-1]
  group.sums.ggl <- c()
  for(j in 1:max(group.index)){
    group.sums.ggl[j] <- sum(abs(ggl.coefs[group.index == j]))
  }
  selected.domains.ggl <- which(group.sums.ggl != 0)
  ngroups[k] <- length(selected.domains.ggl)
  
  # Calculate number of selected features
  nfeatures[k] <- sum(ggl.coefs !=0)
}

# Fit model on complete data
# ggl.obj <- cv.gglasso(feature.matrix, y, group.index, pred.loss="loss", nfolds=10, loss="logit", lambda.factor=0.05, nlambda=100)


## Stop Timer
timer1 <- proc.time()[3]
timer <- round(timer1 - timer0)
seconds <- timer %% 60
minutes <- (timer %/% 60) %% 60
hours <- (timer %/% 60) %/% 60
timer <- c(hours, minutes, seconds)
names(timer) <- c("hours", "minutes", "seconds")

save(ggl.preds, timer, ngroups, nfeatures, file=paste0(TID, ".RData"))

