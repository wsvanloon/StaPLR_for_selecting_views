# Load, process and save colitis data to usable format

library(Biobase)
library(GEOquery)
source("popscale.R")

# Import colitis data
colitis <- getGEO(filename="GDS1615_full.soft.gz")

# Import C1 genesets reference file
c1 <- read.csv("c1.all.v6.1.symbols.gmt", sep="\t", header=F)
c1.matrix <- as.matrix(c1)

# Extract relevant columns and remove duplicate probes
feature.frame <- cbind(NA, Table(colitis)[!duplicated(Table(colitis)$IDENTIFIER), 2:129])

# Map genes from colitis data to C1 genesets
names(feature.frame)[1] <- "GENESET"

for(i in 1:nrow(feature.frame)){
  feature.frame$GENESET[i] <- as.character(c1$V1[row(c1)[match(feature.frame$IDENTIFIER[i], c1.matrix)]])
}

# Remove genes not in C1 genesets
feature.frame <- feature.frame[!is.na(feature.frame$GENESET),]

# Remove lonely genes
feature.frame <- feature.frame[-which(feature.frame$GENESET == names(which(table(feature.frame$GENESET) < 2))), ]

# Create feature matrix of standardized log2-transformed expressions
feature.matrix <- t(feature.frame[,-c(1:2)])
colnames(feature.matrix) <- feature.frame$IDENTIFIER
feature.matrix <- popscale(log2(feature.matrix))

# Vector of integers denoting group (C1 geneset) membership of each feature
group.index <- as.numeric(factor(feature.frame$GENESET))

# Class labels: Normal (0) vs. Colitis/Crohn (1)
y <- ifelse(Columns(colitis)$disease.state == "normal", 0, 1)

save(feature.matrix, group.index, y, file="colitis.RData")
